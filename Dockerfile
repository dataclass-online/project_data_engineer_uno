FROM amazonlinux
RUN yum install epel-release update groupinstall "Development Tools" openssl-devel libffi-devel bzip2-devel xz-devel wget tar gzip make zip unzip gcc -y
RUN yum install awscli -y
RUN wget https://www.python.org/ftp/python/3.9.10/Python-3.9.10.tgz
RUN tar xvf Python-3.9.10.tgz
WORKDIR /Python-3.9.10
RUN ./configure --enable-optimizations
RUN make altinstall
#RUN /usr/local/bin/pip3.9.10 install virtualenv
WORKDIR /
RUN mkdir -p ./ingesta/layers/pandas
RUN mkdir -p ./API/layers/redshift
COPY ./ingesta/layers/pandas /ingesta/layers/pandas 
COPY ./API/layers/redshift /API/layers/redshift
WORKDIR /ingesta/layers/pandas
RUN python3.9 -m pip install -t ./python -r requirements.txt
RUN zip -r pandas_layer.zip python
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG AWS_REGION=us-west-2
RUN aws s3 cp pandas_layer.zip s3://lambdas-aplication-artifacts/layers/pandas_layer.zip
WORKDIR /API/layers/redshift
RUN python3.9 -m pip install -t ./python -r requirements.txt
RUN zip -r redshift_layer.zip python
RUN aws s3 cp redshift_layer.zip s3://lambdas-aplication-artifacts/layers/redshift_layer.zip